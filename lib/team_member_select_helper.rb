# frozen_string_literal: true

require_relative 'www_gitlab_com'

module TeamMemberSelectHelper
  EDITOR_GROUP_REGEXP = /editor/.freeze
  THREAT_INSIGHTS_GROUP_REGEXP = /threat insights/.freeze
  CONTAINER_SECURITY_GROUP_REGEXP = /container security/.freeze
  DYNAMIC_ANALYSIS_GROUP_REGEXP = /dynamic analysis/.freeze
  BACKEND_ENGINEER_REGEXP = /backend engineer\W/.freeze
  FRONTEND_ENGINEER_REGEXP = /frontend engineer\W/.freeze

  def merge_request_coaches(group: nil)
    select_team_members_by_department_specialty_role('merge request coach', nil, group, include_ooo: false)
  end

  def select_random_merge_request_coach(group: nil)
    merge_request_coaches(group: group).sample ||
      # Pick again from the whole pool because we really want someone
      merge_request_coaches.sample
  end

  def untriaged_issues_triagers
    select_team_members_by_department_specialty_role('quality department', nil, /engineering manager/, include_ooo: false)
  end

  def create_editor_be
    @create_editor_be ||= select_random_team_member(EDITOR_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP)
  end

  def create_editor_fe
    @create_editor_fe ||= select_random_team_member(EDITOR_GROUP_REGEXP, nil, FRONTEND_ENGINEER_REGEXP)
  end

  def threat_insights_be
    @threat_insights_be ||= select_random_team_member(THREAT_INSIGHTS_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP, include_ooo: false)
  end

  def threat_insights_fe
    @threat_insights_fe ||= select_random_team_member(THREAT_INSIGHTS_GROUP_REGEXP, nil, FRONTEND_ENGINEER_REGEXP, include_ooo: false)
  end

  def container_security_be
    @container_security_be ||= select_random_team_member(CONTAINER_SECURITY_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP, include_ooo: false)
  end

  def container_security_fe
    @container_security_fe ||= select_random_team_member(CONTAINER_SECURITY_GROUP_REGEXP, nil, FRONTEND_ENGINEER_REGEXP)
  end

  def dast_be
    @dast_be ||= select_random_team_member(DYNAMIC_ANALYSIS_GROUP_REGEXP, nil, BACKEND_ENGINEER_REGEXP)
  end

  private

  def select_random_team_member(department, specialty = nil, role = nil, include_ooo: true)
    picked = select_team_members_by_department_specialty_role(department, specialty, role, include_ooo: include_ooo).sample

    picked ||
      # Pick again from the whole pool because we really want someone
      select_team_members_by_department_specialty_role(department, specialty, role, include_ooo: true).sample
  end

  def select_team_members_by_department_specialty_role(department, specialty = nil, role = nil, include_ooo: true)
    select_team_member_usernames(include_ooo: include_ooo) do |data|
      user_matches?(data, department, specialty, role)
    end
  end

  def user_matches?(data, department, specialty, role)
    department, department_matcher = field_and_matcher(department)
    specialty, specialty_matcher = field_and_matcher(specialty)
    role, role_matcher = field_and_matcher(role)

    [].tap do |results|
      results << Array(data['departments']).any?(&department_matcher) if department
      results << Array(data['specialty']).any?(&specialty_matcher) if specialty
      results << role_matcher.call(data['role']&.downcase) if role
    end.all?
  end

  def field_and_matcher(raw_field)
    if raw_field.is_a?(Regexp)
      field = Regexp.new(raw_field.source, Regexp::IGNORECASE)
      [
        field,
        ->(value) { value =~ field }
      ]
    else
      field = raw_field&.downcase
      [
        field,
        ->(value) { value.downcase == field }
      ]
    end
  end

  def select_team_member_usernames(include_ooo: true)
    WwwGitLabCom.team_from_www.each_with_object([]) do |(username, data), memo|
      next if out_of_office?(username) unless include_ooo

      memo << "@#{username}" if yield(data)
    end
  end

  def out_of_office_team_members
    @out_of_office_team_members ||= WwwGitLabCom.roulette
      .filter_map { |data| data['username'] if data.fetch('out_of_office', false) }
  end

  def out_of_office?(username)
    out_of_office_team_members.include?(username)
  end
end
