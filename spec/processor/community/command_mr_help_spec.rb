# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_help'

RSpec.describe Triage::CommandMrHelp do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        new_comment: %(@gitlab-bot help)
      }
    end
  end

  let(:coach_username) { '@coach_username' }
  let(:by_team_member) { false }

  subject { described_class.new(event) }

  before do
    allow(subject).to receive(:select_random_merge_request_coach).and_return(coach_username)
    allow(event).to receive(:by_team_member?).and_return(by_team_member)
  end

  include_examples 'registers listeners', ['merge_request.note']
  include_examples 'command processor', 'help'

  describe '#applicable?' do
    include_context 'with community contribution command processor'

    it_behaves_like 'community contribution command processor #applicable?'

    context 'when actor is not a team member' do
      it_behaves_like 'rate limited', count: 1, period: 3600
    end

    context 'when actor is a team member' do
      let(:by_team_member) { true }

      it_behaves_like 'rate limited', count: 100, period: 3600
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment to reply to author and pings MR coaches' do
      body = <<~MARKDOWN.chomp
        Hey there #{coach_username}, could you please help @root out?
        /assign_reviewer #{coach_username}
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
