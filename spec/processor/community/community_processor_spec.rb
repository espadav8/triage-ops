# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/community_processor'

RSpec.describe Triage::CommunityProcessor do
  using RSpec::Parameterized::TableSyntax

  include_context 'with event', 'Triage::MergeRequestEvent'

  describe '#wider_community_contribution?' do
    subject { described_class.new(event) }

    context 'without the "Community contribution" label' do
      it 'returns false' do
        expect(subject.wider_community_contribution?).to eq(false)
      end
    end

    context 'with the "Community contribution" label' do
      let(:label_names) { [described_class::COMMUNITY_CONTRIBUTION_LABEL] }

      it 'returns true' do
        expect(subject.wider_community_contribution?).to eq(true)
      end
    end
  end

  describe '#wider_community_contribution_open_resource?' do
    where(:wider_community_contribution, :resource_open, :expected_result) do
      # When wider_community_contribution: true and resource_open: true => true
      true | true | true

      # When wider_community_contribution: false => false
      false | true | false

      # When wider_community_contribution: true and resource_open: false => false
      true | false | false
    end

    with_them do
      include_context 'with event', 'Triage::MergeRequestEvent' do
        let(:event_attrs) do
          {
            resource_open?: resource_open
          }
        end
      end

      subject { described_class.new(event) }

      before do
        allow(subject).to receive(:wider_community_contribution?).and_return(wider_community_contribution)
      end

      it 'returns the expected result' do
        expect(subject.wider_community_contribution_open_resource?).to eq(expected_result)
      end
    end
  end

  describe '#wider_community_contribution_valid_command?' do
    let(:processor) do
      Class.new(Triage::CommunityProcessor) do
        define_command name: 'foo'
      end
    end

    subject { processor.new(event) }

    before do
      allow(subject).to receive(:wider_community_contribution?).and_return(wider_community_contribution)

      command_stub = double(valid?: command_valid)
      allow(subject).to receive(:command).and_return(command_stub)
    end

    where(:wider_community_contribution, :command_valid, :by_noteable_author, :by_team_member, :expected_result) do
      # When wider_community_contribution: false => false
      false | false | false | false | false
      false | false | false | true | false
      false | false | true | false | false
      false | false | true | true | false
      false | true | false | false | false
      false | true | false | true | false
      false | true | true | false | false
      false | true | true | true | false

      # When command_valid: false => false
      false | false | false | false | false
      false | false | false | true | false
      false | false | true | false | false
      false | false | true | true | false
      true | false | false | false | false
      true | false | false | true | false
      true | false | true | false | false
      true | false | true | true | false

      # When wider_community_contribution: true and command_valid: true
      true | true | false | false | false
      true | true | false | true | true
      true | true | true | false | true
      true | true | true | true | true
      true | true | false | false | false
      true | true | false | true | true
      true | true | true | false | true
      true | true | true | true | true
    end

    with_them do
      include_context 'with event', 'Triage::MergeRequestEvent' do
        let(:event_attrs) do
          {
            by_noteable_author?: by_noteable_author,
            by_team_member?: by_team_member
          }
        end
      end

      it 'returns the expected result' do
        expect(subject.wider_community_contribution_valid_command?).to eq(expected_result)
      end
    end
  end
end
