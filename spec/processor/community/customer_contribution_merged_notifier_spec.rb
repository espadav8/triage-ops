# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/customer_contribution_merged_notifier'

RSpec.describe Triage::CustomerContributionMergedNotifier do
  include_context 'with slack posting context'
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        type_label: contribution_type,
        url: url
      }
    end
    let(:contribution_type) { 'type::bug' }
    let(:url) { 'http://gitlab.com/mr_url' }
  end

  let(:org_name) { 'org' }

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(Triage::OrgByUsernameLocator).to receive(:locate_org).and_return(org_name)
    allow(messenger_stub).to receive(:ping)
  end

  include_examples 'registers listeners', ['merge_request.merge']
  include_examples 'processor slack options', '#mrarr-wins'

  describe '#applicable?' do
    it_behaves_like 'community contribution processor #applicable?'
    it_behaves_like 'customer contribution processor #applicable?'
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:message_body) do
      <<~MARKDOWN
        > Customer MR Merged -
        > Organization: #{described_class::CUSTOMER_PORTAL_URL}#{org_name}
        > Contribution Type: #{contribution_type}
        > MR Link: #{url}
      MARKDOWN
    end

    it_behaves_like 'customer contribution processor #process'
  end
end
