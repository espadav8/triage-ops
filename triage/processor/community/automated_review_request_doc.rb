# frozen_string_literal: true

require_relative 'community_processor'
require_relative 'automated_review_request_generic'

require_relative '../../triage/documentation_code_owner'

module Triage
  class AutomatedReviewRequestDoc < CommunityProcessor
    DOC_FILE_REGEX = %r{\Adocs?/}
    DOCUMENTATION_LABEL = 'documentation'
    TECHNICAL_WRITING_LABEL = 'Technical Writing'
    TECHNICAL_WRITING_TRIAGED_LABEL = 'tw::triaged'
    GL_DOCSTEAM_HANDLE = 'gl-docsteam'

    react_to 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        !merge_request_labelled_technical_writing? &&
        workflow_ready_for_review_added? &&
        merge_request_changes_doc? &&
        unique_comment.no_previous_comment?
    end

    def process
      post_documentation_label_comment
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def workflow_ready_for_review_added?
      event.added_label_names.include?(AutomatedReviewRequestGeneric::WORKFLOW_READY_FOR_REVIEW_LABEL)
    end

    def merge_request_changes_doc?
      merge_request_changes.any? do |change|
        doc_change?(change)
      end
    end

    def merge_request_labelled_technical_writing?
      event.label_names.any? do |label|
        label == TECHNICAL_WRITING_LABEL ||
        label.start_with?('tw::')
      end
    end

    def doc_change?(change)
      %w[old_path new_path].any? do |path|
        change[path].match(DOC_FILE_REGEX)
      end
    end

    def merge_request_changes
      Triage.api_client.merge_request_changes(project_id, merge_request_iid).changes
    end

    def project_id
      event.project_id
    end

    def merge_request_iid
      event.iid
    end

    def post_documentation_label_comment
      comment = <<~MARKDOWN.chomp
        #{message if approver_usernames.any?}
        /label ~"#{DOCUMENTATION_LABEL}" ~"#{TECHNICAL_WRITING_TRIAGED_LABEL}"
      MARKDOWN

      add_comment(comment.strip)
    end

    def message
      comment = <<~MESSAGE
        Hi #{approver_usernames.join(' ')}! Please review this ~"#{DOCUMENTATION_LABEL}" merge request.
      MESSAGE

      if approver_usernames.include?("@#{GL_DOCSTEAM_HANDLE}")
        comment += <<~MESSAGE

          Please also consider updating the `CODEOWNERS` file in the #{event.project_web_url} project.
        MESSAGE
      end

      unique_comment.wrap(comment)
    end

    def approver_usernames
      approvers = Triage::DocumentationCodeOwner.new(project_id, merge_request_iid).approvers
      approvers.map { |username| "@#{username}" }
    end
  end
end
