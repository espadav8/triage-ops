# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class CodeReviewExperienceFeedback < CommunityProcessor

    react_to 'merge_request.merge', 'merge_request.close'

    def applicable?
      wider_community_contribution? &&
        unique_comment.no_previous_comment?
    end

    def process
      post_review_experience_comment
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def post_review_experience_comment
      add_comment(message.strip)
    end

    def message
      comment =  <<~MARKDOWN.chomp
        @#{event.resource_author.username}, how was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://about.gitlab.com/handbook/values/) and improve:

        1. Start a new comment with `@gitlab-bot feedback`
        1. Include :thumbsup: or a :thumbsdown: to describe your experience.
        1. Leave any additional feedback you have for us in the comment.

        Have five minutes? Take our [survey](https://forms.gle/g26h8uEKgvTLGSQw6) to give us even more feedback on how GitLab can improve the contributor experience.

        Thanks for your help! :heart:
      MARKDOWN

      unique_comment.wrap(comment)
    end
  end
end
