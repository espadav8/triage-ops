# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'

module Triage
  class CommunityProcessor < Processor
    COMMUNITY_CONTRIBUTION_LABEL = 'Community contribution'

    def wider_community_contribution?
      event.label_names.include?(COMMUNITY_CONTRIBUTION_LABEL)
    end

    def wider_community_contribution_open_resource?
      event.resource_open? &&
        wider_community_contribution?
    end

    def wider_community_contribution_valid_command?
      command.valid?(event) &&
        wider_community_contribution? &&
        (event.by_noteable_author? || event.by_team_member?)
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
