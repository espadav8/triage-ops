# frozen_string_literal: true

require 'time'

require_relative 'community_processor'

module Triage
  class HackathonLabel < CommunityProcessor
    HACKATHON_LABEL = 'Hackathon'
    HACKATHON_START_DATE = Time.parse('2022-05-09T12:00:00Z')
    HACKATHON_END_DATE = Time.parse('2022-05-13T12:00:00Z')
    HACKATHON_TRACKING_ISSUE = 'https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/-/issues/65'
    HACKATHON_WEBPAGE = 'https://about.gitlab.com/community/hackathon/'

    react_to 'merge_request.open', 'merge_request.update'

    def applicable?
      wider_community_contribution_open_resource? &&
        created_during_hackathon? &&
        # Ensure we don't post the note twice since we only introduced `unique_comment` recently.
        # This could be removed after some time.
        no_hackathon_label? &&
        unique_comment.no_previous_comment?
    end

    def process
      label_hackathon
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def label_hackathon
      add_comment(<<~MARKDOWN.chomp)
        This merge request will be considered [part of](#{HACKATHON_TRACKING_ISSUE}) the quarterly [GitLab Hackathon](#{HACKATHON_WEBPAGE}) for a chance to win a [prize](#{HACKATHON_WEBPAGE}#prize).

        Can you make sure this merge request mentions or links to the relevant issue that it's attempting to close?

        Thank you for your contribution!

        /label ~"#{HACKATHON_LABEL}"
      MARKDOWN
    end

    def no_hackathon_label?
      !event.label_names.include?(HACKATHON_LABEL)
    end

    def created_during_hackathon?
      event.created_at >= HACKATHON_START_DATE &&
        event.created_at < HACKATHON_END_DATE
    end
  end
end
