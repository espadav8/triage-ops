# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class RemoveIdleLabelOnActivity < CommunityProcessor
    IDLE_LABEL = 'idle'
    STALE_LABEL = 'stale'

    react_to 'merge_request.update', 'merge_request.note'

    def applicable?
      wider_community_contribution? &&
        event.by_noteable_author? &&
        idle_or_stale? &&
        # Either the event is a note event, or it's a revision update
        (!event.respond_to?(:revision_update?) || event.revision_update?)
    end

    def process
      add_comment <<~COMMENT.chomp
        /unlabel ~"#{IDLE_LABEL}" ~"#{STALE_LABEL}"
      COMMENT
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def idle_or_stale?
      (event.label_names & [IDLE_LABEL, STALE_LABEL]).any?
    end
  end
end
