# frozen_string_literal: true

require_relative 'community_processor'

require_relative '../../strings/thanks'

module Triage
  class ThankContribution < CommunityProcessor
    GITLAB_RUNNER_PROJECT_ID = 250_833
    WWW_GITLAB_COM_PROJECT_ID = 7764
    GITLAB_PROJECT_ID = 278_964
    GITLAB_FOSS_PROJECT_ID = 13_083

    PROJECT_THANKS = {
      GITLAB_RUNNER_PROJECT_ID => {
        message: Strings::Thanks::RUNNER_THANKS
      },
      WWW_GITLAB_COM_PROJECT_ID => {
        extra_conditions: ->(processor) {
          event.from_www_gitlab_com? &&
            event.wider_gitlab_com_community_author?
        },
        message: Strings::Thanks::WWW_GITLAB_COM_THANKS
      },
      GITLAB_PROJECT_ID => {
        message: Strings::Thanks::GITLAB_THANKS
      },
      GITLAB_FOSS_PROJECT_ID => {
        message: Strings::Thanks::GITLAB_FOSS_THANKS
      }
    }

    DEFAULT_EXTRA_CONDITIONS = ->(processor) {
      event.from_gitlab_org? &&
        event.wider_gitlab_org_community_author?
    }

    react_to 'merge_request.open'

    def applicable?
      extra_conditions
    end

    def process
      post_thank_you_message
    end

    def documentation
      <<~TEXT
        # TODO: Add actual documentation
      TEXT
    end

    private

    def post_thank_you_message
      thank_you_message = PROJECT_THANKS.dig(event.project_id, :message) || Strings::Thanks::DEFAULT_THANKS
      add_comment format(thank_you_message, author_username: event.event_actor_username)
    end

    def extra_conditions
      extra_conditions_lambda = PROJECT_THANKS.dig(event.project_id, :extra_conditions)
      extra_conditions_lambda ? instance_eval(&extra_conditions_lambda) : instance_eval(&DEFAULT_EXTRA_CONDITIONS)
    end
  end
end
