# frozen_string_literal: true

require 'rack'
require_relative '../job/processor_job'

module Triage
  module Rack
    class Processor
      def call(env)
        ::Triage::ProcessorJob.perform_async(env[:payload])

        ::Rack::Response.new([JSON.dump(status: :ok)]).finish
      end
    end
  end
end
