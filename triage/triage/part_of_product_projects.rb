# frozen_string_literal: true

require 'csv'

require_relative '../triage'

module Triage
  class PartOfProductProjects
    CSV_URLS = {
      com: 'https://gitlab.com/gitlab-data/analytics/-/raw/master/transform/snowflake-dbt/data/projects_part_of_product.csv',
      ops: 'https://gitlab.com/gitlab-data/analytics/-/raw/master/transform/snowflake-dbt/data/projects_part_of_product_ops.csv'
    }
    CSV_CACHE_EXPIRY = 3600 * 4

    def self.part_of_product_projects(instance_key)
      new.csv_map(instance_key)
    end

    def csv_map(instance_key)
      ::Triage.cache.get_or_set("part_of_product_projects-#{instance_key}", expires_in: CSV_CACHE_EXPIRY) do
        retrieve_csv(CSV_URLS.fetch(instance_key))
          .map(&:to_hash)
          .each { |hash| hash['project_id'] = hash['project_id'].to_i }
      end
    end

    private

    def retrieve_csv(csv_url)
      response = HTTParty.get(csv_url)
      CSV.parse(response.parsed_response, headers: :first_row)
    end
  end
end
